#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
from functools import partial


parser = argparse.ArgumentParser(
  description="""
  Generate a file of commands to run (for parallel for instance).

  Each line specifies parameters for the program to run on.
  The parameter values to cover are specified using '--value' for a fixed
  parameter, '--range' for a range of int values, and '--list' for a list
  of any values.

  Additionally it is possible to construct new parameters from other ones
  using '--add', '--scale' and '--format-args'.
  You can use '--dont-pass' to use a parameter only as a proxy to build
  other values, without passing it on to the final commands.

  Unrecognized arguments are passed directly at the start of the command lines.

  """
)


parser.add_argument(
  "-v",
  "--value",
  nargs=2,
  action="append",
  default=[],
  metavar=("name", "val"),
  help="Pass --'name'='val' as argument for all lines",
)

parser.add_argument(
  "-r",
  "--range",
  nargs=3,
  action="append",
  default=[],
  metavar=("name", "start", "end"),
  help="Pass --'name'=k for k from 'start' to 'end'",
)

parser.add_argument(
  "--list",
  nargs="+",
  action="append",
  default=[],
  metavar=("name", "values"),
  help="Pass --'name'=v for v in 'values'",
)

parser.add_argument(
  "--scale",
  nargs=3,
  action="append",
  default=[],
  metavar=("new-name", "factor", "base-name"),
  help="Pass --'new-name'=x where x is 'factor' times the value of 'base-name' (which must also be given).",
)

parser.add_argument(
  "--add",
  nargs=3,
  action="append",
  default=[],
  metavar=("new-name", "incr", "base-name"),
  help="Pass --'new-name'=x where x is 'incr' + 'base-name' (which must also be given).",
)

parser.add_argument(
  "--format-args",
  nargs=2,
  action="append",
  default=[],
  metavar=("new-name", "format-string"),
  help="Pass --'new-name'=v where v is the result of formatting 'format-string' with the argument values.",
)

parser.add_argument(
  "--dont-pass",
  action="append",
  default=[],
  help="Do not pass this argument on to the final command line.",
)


def make_range(rg):
  name, start, end = rg
  return (name, range(int(start), int(end) + 1))


def make_list(l):
  name = l[0]
  values = l[1:]
  return (name, values)


def make_scale(sc):
  new, factor, base = sc
  return (new, float(factor), base)


def make_add(a):
  new, incr, base = a
  return (new, int(incr), base)


def make_line(lead, values):
  def fmt(v):
    if isinstance(v, float):
      return "--{name}={value:g}"
    elif isinstance(v, str) and " " in v:
      # For the case where v is actually a list of values.
      # How could we recognize it better ?
      return "--{name} {value}"
    else:
      return "--{name}={value}"

  line = " ".join(lead + [fmt(v).format(name=nm, value=v) for (nm, v) in values])
  return line


def print_complete_line(lead, scales, adds, format_args, dont_pass, **vars):
  # we decide that 'add' are bound first, then 'scales' then 'format_args',
  # in each case in the order of the command line
  # subsequent ones can depend on previous ones.

  values = list(vars.items())

  # adds
  for new, incr, base in adds:
    try:
      vars[base]
    except KeyError:
      raise ValueError("No parameter", base, "to add to")
    v = incr + int(vars[base])
    vars[new] = v
    values.append((new, v))

  # scales
  for new, factor, base in scales:
    try:
      vars[base]
    except KeyError:
      raise ValueError("No parameter", base, "to rescale")
    v = factor * float(vars[base])
    vars[new] = v
    values.append((new, v))

  # format_args
  for new, fmt in format_args:
    v = fmt.format(**vars)
    vars[new] = v
    values.append((new, v))

  # filter out dont_pass
  values = [(nm, v) for (nm, v) in values if nm not in dont_pass]

  print(make_line(lead, values))


def loop_values(iters, f, **kw):
  name, it = iters[0]
  # loop on it
  for x in it:
    # call f with name=x
    args = {name: x}
    if len(iters) == 1:
      # no remaining args to iterate
      # pass last arg and remaining fixed args to finish calling f
      f(**{**args, **kw})
    else:
      # iterate on next arg
      # (copy iters to use it as if it was immutable
      loop_values(iters[1:], partial(f, **args), **kw)


def main():
  args, unknown_args = parser.parse_known_args()

  values = {name: v for (name, v) in args.value}
  ranges = [make_range(rg) for rg in args.range]
  lists = [make_list(li) for li in args.list]
  scales = [make_scale(sc) for sc in args.scale]
  adds = [make_add(a) for a in args.add]

  iters = ranges + lists
  if len(iters) == 0:
    print_complete_line(
      lead=unknown_args,
      scales=scales,
      adds=adds,
      format_args=args.format_args,
      dont_pass=args.dont_pass,
      **values,
    )
  else:
    loop_values(
      ranges + lists,
      f=print_complete_line,
      lead=unknown_args,
      scales=scales,
      adds=adds,
      format_args=args.format_args,
      dont_pass=args.dont_pass,
      **values,
    )


if __name__ == "__main__":
  sys.exit(main())
