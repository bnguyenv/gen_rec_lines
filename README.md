Gen_rec_lines
=============

Output a list of lines (as a `\n` separated string) to standard output by looping over variables.

The goal of this small library is to easily generate input files for GNU parallel for instance,
by filling files with lines looping over some variables, defined by lists or ranges of values,
or from other variables.

See `gen_rec_lines --help` (or `python3 gen_rec_lines.py --help`) for more.
